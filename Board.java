import java.util.Random;
public class Board{
	//fields
	private Tile[][] grid;
	private final int SIZE = 5;                                                                                                                    
	//constructor
	public Board(){
		this.grid = new Tile[SIZE][SIZE];
		Random rand = new Random();
		for(int i = 0; i < grid.length; i++){ 
			int randPosition = rand.nextInt(grid[i].length);
			for(int j = 0; j < grid[i].length; j++){
				if(j == randPosition){
					grid[i][j] = Tile.HIDDEND_WALL;
				}
				else{
					grid[i][j] = Tile.BLANK;
				}
 			}
		}
	}
	//represent the board visualy
	public String toString(){
		String gridDisplay = "";
		for(int i = 0; i < grid.length; i++){ //checks each ROW of the grid (3 x 3)
			for(int j = 0; j < grid[i].length; j++){ //checks each COLUMN of the grid 
				gridDisplay += this.grid[i][j] + " ";
			}
			gridDisplay += "\n";
		}
		return gridDisplay;
	}
	//instance method : based on which instance was used to call the method 
	// in this method we are checking if what the player chose was valid and
	// if that spot it blank you can take it else false (case like the spot was alredy taken)
	public int placeToken(int row, int col){
		if ((row <= 5) || (col <= 5)){
			return -2;
		}
		else if(grid[row][col] == Tile.CASTLE || grid[row][col] == Tile.WALL){
			return -1;
		}
		else if(grid[row][col] == Tile.HIDDEND_WALL){
			grid[row][col] = Tile.WALL;
			return 1;
		}
		else{
			grid[row][col] = Tile.CASTLE;
			return 0;
		}
	}
}

	