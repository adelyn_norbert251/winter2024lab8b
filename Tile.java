public enum Tile{
	BLANK("_"),
	WALL("W"),
	HIDDEND_WALL("_"),
	CASTLE("C"),
	X("X"),
	O("O");
	private String name;
	private Tile (String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
	
	
}
